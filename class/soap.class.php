<?php

class ServiceFunctions
{
    public $db;

    public function __construct()
    {
        $dbc['host'] = "localhost";
        $dbc['user'] = "root";
        $dbc['pass'] = "";
        $dbc['name'] = "iai";
        $dbc['encode'] = "SET NAMES utf8";

        $dbc['dns'] = 'mysql:host=' . $dbc['host'] . ';dbname=' . $dbc['name'];
        $dbc['options'] = array(
            PDO::MYSQL_ATTR_INIT_COMMAND => $dbc['encode'],
        );
        try {
            $this->db = new PDO($dbc['dns'], $dbc['user'], $dbc['pass'], $dbc['options']);
            $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            die('Wystąpił błąd przy łączeniu z bazą danych.');
        }
        unset($dbc);

    }

    public function getInitials($first_name, $last_name)
    {
        $name = '';
        $name .= strtoupper(substr($first_name, 0, 1));
        $name .= '' . strtoupper(substr($last_name, 0, 1));
        return $name;
    }


    public function getAllSoap()
    {
        $result = $this->db->prepare("SELECT id, name, price FROM products");
        $result->execute();

        while ($row = $result->fetch()) {
            $array[] = $row['name'] . ' ' . $row['price'];
        }
        return $array;

    }
}