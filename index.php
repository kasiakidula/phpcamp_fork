﻿<?php //wprowadzę jakieś zmiany xd

$dbc['host'] = "localhost";
$dbc['user'] = "root";
$dbc['pass'] = "";
$dbc['name'] = "iai";
$dbc['encode'] = "SET NAMES utf8";

$dbc['dns'] = 'mysql:host=' . $dbc['host'] . ';dbname=' . $dbc['name'];
$dbc['options'] = array(
    PDO::MYSQL_ATTR_INIT_COMMAND => $dbc['encode'],
);
try {
    $db = new PDO($dbc['dns'], $dbc['user'], $dbc['pass'], $dbc['options']);
    $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
} catch (PDOException $e) {
    die('Wystąpił błąd przy łączeniu z bazą danych.');
}
unset($dbc);

if ($_GET) {

    if ($_GET['action'] == 'addProduct') {
        $result = $db->prepare("INSERT INTO `products` (`id`, `name`, `price`) VALUES
('', :name, :price)");

        $result->bindValue(':name', $_GET['name'], PDO::PARAM_INT);
        $result->bindValue(':price', $_GET['price'], PDO::PARAM_INT);
        $result->execute();
    }

    if ($_GET['action'] == 'checkProduct') {
        $result = $db->prepare("SELECT id, name, price FROM products WHERE name = :name");
        $result->bindValue(':name', $_GET['name'], PDO::PARAM_INT);
        $result->execute();

        $simple = new SimpleXMLElement(('<?xml version="1.0"?><list />'));

        while ($row = $result->fetch()) {
            $simple->addChild($row['name'], $row['price']);

        }
        echo $simple->asXML();
    }


    if ($_GET['action'] == 'allProduct') {
        $result = $db->prepare("SELECT id, name, price FROM products");
        $result->execute();

        $simple = new SimpleXMLElement(('<?xml version="1.0"?><list />'));

        while ($row = $result->fetch()) {
            $simple->addChild('id', $row['id']);
            $simple->addChild('name', $row['name']);
            $simple->addChild('price', $row['price']);

        }
        echo $simple->asXML();
    }


    if ($_GET['action'] == 'removeProduct') {
        $result = $db->prepare("DELETE FROM `products` WHERE id = :id");
        $result->bindValue(':id', $_GET['id'], PDO::PARAM_INT);
        $result->execute();
    }


}

if (!isset($_GET['clear'])) {
    echo '
<br><br><br>
allProduct<br>
checkProduct<br>
addProduct<br>
removeProduct<br>
<br><br><br>
<form method="GET">

    action: <input name="action" value=""/>
    name: <input name="name"/>
    price: <input name="price"/>
    id: <input name="id"/>

    <input type="submit"/>
</form>
';
}


?>
