<?php

$ch = curl_init('localhost/iai/index.php?action=allProduct&clear=true');
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
$result = curl_exec($ch);

//header("Content-type: text/xml");
//echo $result;

function XML2Array(SimpleXMLElement $parent)
{
    $array = array();

    foreach ($parent as $name => $element) {
        ($node = & $array[$name])
        && (1 === count($node) ? $node = array($node) : 1)
        && $node = & $node[];

        $node = $element->count() ? XML2Array($element) : trim($element);
    }

    return $array;
}

$xml   = simplexml_load_string($result);
$array = XML2Array($xml);
$array = array($xml->getName() => $array);


print_r($array);